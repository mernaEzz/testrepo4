import {BrowserRouter, Route} from 'react-router-dom'; 
import Form from './components/form/form';
import Home from './components/home/home';
import Payment from './components/payment/payment';
import Stripe from './components/stripe/stripe';
import Stripe2 from './components/stripe2/stripe2';
import StripeFull from './components/stripeConnect/stripeFull';
import StripeInput from './components/StripeInput/StripeInput';
import stripePayment from './components/stripePayment/stripePayment';
import stripeSkate from './components/stripeSkate/stripeSkate';
 
function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Route exact path="/" component={Home}/>
      <Route exact path="/stripe" component={Stripe}/>
      <Route exact path="/form" component={Form}/>
      <Route exact path="/s" component={stripePayment}/>
      <Route exact path="/st" component={stripeSkate}/>
      <Route exact path="/stripeConnect" component={StripeFull}/>

      </BrowserRouter>
       
     </div>
  );
}

export default App;
