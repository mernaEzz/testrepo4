import { extend } from "jquery";
import React from "react";
import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import CheckoutForm from "./checkoutForm";
import { Link , Redirect, withRouter} from "react-router-dom";

const stripePromise = loadStripe("pk_test_TYooMQauvdEDq54NiTphI7jx");

class StripeFull extends React.Component {
 
  go=()=>{
   this.props.history.push('/d')
  }
  render() {
    return (
      <div>
        <Elements stripe={stripePromise}>
          <CheckoutForm go={this.go}/>
        </Elements>
      </div>
    );
  }
}
export default withRouter(StripeFull)
