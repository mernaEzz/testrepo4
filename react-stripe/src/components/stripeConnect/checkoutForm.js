import React from "react";
import {
  ElementsConsumer,
  CardElement,
  CardNumberElement,
  CardExpiryElement,
  CardCvcElement,
} from "@stripe/react-stripe-js";
import "./checkoutForm.css";
import $ from "jquery";
import Visa from "../../images/Group 1905@2x.png";
import Mastercard from "../../images/Group 1906.png";
import Amex from "../../images/Mask Group 7.png";
import Discover from "../../images/Group 1908.png";
import { Link, Redirect, withRouter } from "react-router-dom";

const Logo = ({ type, alt, active }) => {
  let imgClass = "ximg";

  if (active) {
    imgClass = "ximg  active";
  }

  return (
    <>
      <img src={type} alt={`${alt} credit card logo`} className={imgClass} />
    </>
  );
};

const CARD_ELEMENT_OPTIONS = {
  style: {
    base: {
      color: "#32325d",
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSmoothing: "antialiased",
      fontSize: "16px",
      "::placeholder": {
        color: "white",
      },
    },
    invalid: {
      color: "#ff0000c4",
      iconColor: "#fa755a",
    },
  },
};

class CheckoutForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      nameOnCard: "",
      cardNumber: "",
      cardExpiry: "",
      cardCvv: "",
    };
  }
  emailChanges = (event) => {
    const email = this.state.email;
    this.setState({
      email: event.target.value,
    });
    var displayError = document.getElementById("email-error");
    const expression = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var validEmail = expression.test(String(event.target.value).toLowerCase());
    if (!validEmail) {
      displayError.innerHTML = "email is invalid";
    } else {
      displayError.innerHTML = "";
    }
    if (event.target.value === "") {
      displayError.innerHTML = "";
    }
  };

  nameChanges = (event) => {
    const nameOnCard = this.state.nameOnCard;
    this.setState({
      nameOnCard: event.target.value,
    });
    var displayError = document.getElementById("name-error");
    const expression = /^([a-zA-Z]+[\s'.]?)+\S$/;
    var validName = expression.test(String(event.target.value).toLowerCase());
    if (!validName) {
      displayError.innerHTML = "name is incomplete or invalid";
    } else {
      displayError.innerHTML = "";
    }
    if (event.target.value === "") {
      displayError.innerHTML = "";
    }
  };
  cardNumberChanges = (event2) => {
    console.log(event2.empty);
    var displayError = document.getElementById("card-number-error");
    if (event2.error) {
      displayError.innerHTML = event2.error.message;
    } else {
      displayError.innerHTML = "";
    }
    if (event2.complete) {
      displayError.innerHTML = "";
    }
  };

  cardExpiryChanges = (event) => {
    var displayError = document.getElementById("card-expiry-error");
    if (event.error) {
      displayError.innerHTML = event.error.message;
    } else {
      displayError.innerHTML = "";
    }
  };
  cardCvvChanges = (event) => {
    var displayError = document.getElementById("cvv-error");
    if (event.error) {
      displayError.innerHTML = event.error.message;
    } else {
      displayError.innerHTML = "";
    }
  };
 
  handleSubmit = async (event) => {
    // We don't want to let default form submission happen here,
    // which would refresh the page.
    event.preventDefault();
     const { email, nameOnCard,cardNumber, cardExpiry, cardCvv } = this.state;
    var nameError = document.getElementById("name-error");
    var emailError = document.getElementById("email-error");
    var cardExpiryErrors = document.getElementById("card-expiry-error");
    var cardNumberErrors = document.getElementById("card-number-error");
    var cardCvvErrors = document.getElementById("cvv-error");

    if (nameOnCard === "") {
      nameError.innerHTML = "name is required";
    } else {
      nameError.innerHTML = "";
    }
    if (email === "") {
      emailError.innerHTML = "email is required";
    } else {
      emailError.innerHTML = "";
    } 

    //card number
    const element = document.querySelector("#cardNumber");

    if (cardNumber === "") {
      cardNumberErrors.innerHTML = "Card Number is required";
    } else {
      cardNumberErrors.innerHTML = "";
    } 
   if($("#cardNumber").hasClass("StripeElement--complete")||$("#cardNumber").hasClass("StripeElement---invalid")){
    cardNumberErrors.innerHTML = "";
   }

   if (cardExpiry === "") {
    cardExpiryErrors.innerHTML = "Card Expiry is required";
  } else {
    cardExpiryErrors.innerHTML = "";
  } 
 if($("#cardExpiry").hasClass("StripeElement--complete")||$("#cardExpiry").hasClass("is-invalid")){
  cardExpiryErrors.innerHTML = "";
 }
 


 if (cardCvv === "") {
  cardCvvErrors.innerHTML = "Card Cvv is required";
} else {
  cardCvvErrors.innerHTML = "";
} 
if($("#cardCvv").hasClass("StripeElement--complete")||$("#cardCvv").hasClass("is-invalid")){
  cardCvvErrors.innerHTML = "";
}

    const { stripe, elements } = this.props;
    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make  sure to disable form submission until Stripe.js has loaded.
      return;
    }

    // const result = await stripe.confirmCardPayment("{CLIENT_SECRET}", {
    //   payment_method: {
    //     card: elements.getElement(CardElement),
    //     billing_details: {
    //       name: "",
    //     },
    //   },
    // });

    // if (result.error) {
    //   // Show error to your customer (e.g., insufficient funds)
    //   console.log(result.error.message);
    // } else {
    //   // The payment has been processed!
    //   if (result.paymentIntent.status === "succeeded") {
    //     // Show a success message to your customer
    //     // There's a risk of the customer closing the window before callback
    //     // execution. Set up a webhook or plugin to listen for the
    //     // payment_intent.succeeded event that handles any business critical
    //     // post-payment actions.
    //   }
    // }
  };

  //   <CardElement options={CARD_ELEMENT_OPTIONS} />
  render() {
    return (
      <div className="checkoutForm col-md-6">
        <div className="payHeader container">
          <button className="container">
            <i className="fa fa-apple"></i> Pay
          </button>
        </div>
        <div className="or container">
          <p>Or pay with card</p>
        </div>
        <form
          onSubmit={this.handleSubmit}
          className="Form container"
          noValidate
          id="forms"
        >
          <fieldset className="FormGroup">
            <div className="input-group">
              <div className="form-group">
                <input
                  id="email"
                  type="text"
                  className="form-control"
                  required
                  onChange={this.emailChanges}
                />
                <label>email</label>
              </div>
            </div>
            <div id="email-error"></div>
          </fieldset>
          <br />
          <fieldset className="FormGroup">
            <div className="input-group">
              <div className="input-group-append">
                <div className="form-group">
                  <CardNumberElement
                    options={CARD_ELEMENT_OPTIONS}
                    id="cardNumber"
                    onChange={this.cardNumberChanges}
                   />
                  <label>card number</label>
                </div>
                <Logo type={Visa} alt="Visa" />
                <Logo type={Mastercard} alt="Mastercard" />
                <Logo type={Amex} alt="American Express" />
                <Logo type={Discover} alt="Discover" />
              </div>
            </div>
            <div id="card-number-error"></div>
          </fieldset>
          <br />
          <fieldset className="FormGroup">
            <div className="input-group">
              <div className="form-group">
                <input
                  id="nameOnCard"
                  type="text"
                  className="form-control"
                  required
                  onChange={this.nameChanges}
                />
                <label>name on card</label>
              </div>
            </div>
            <div id="name-error"></div>
          </fieldset>
          <br />
          <fieldset className="FormGroup row">
            <div className="col-md-6">
              <div className="input-group">
                <div className="form-group">
                  <CardExpiryElement
                    options={CARD_ELEMENT_OPTIONS}
                    id="cardExpiry"
                    onChange={this.cardExpiryChanges}
                  />

                  <label>Expiry Date</label>
                </div>
              </div>
              <div id="card-expiry-error"></div>
            </div>
            <div className="col-md-6">
              <div className="input-group">
                <div className="input-group-append">
                  <div className="form-group">
                    <CardCvcElement
                      options={CARD_ELEMENT_OPTIONS}
                      id="cardCvv"
                      onChange={this.cardCvvChanges}
                    />
                    <label>cvv</label>
                  </div>
                  <div className="items">
                    <a href="#" className="has-tooltip">
                      <img src="Mask Group 8.svg" id="example" />
                      <div className="container">
                        <span className="tooltip tooltip-top ">
                          is the three-digit number at the back of your debit
                          card. For certain types of debit cards, it could be a
                          four-digit number printed on the front
                        </span>
                      </div>
                    </a>
                  </div>
                </div>
              </div>{" "}
              <div id="cvv-error"></div>
            </div>
          </fieldset>
          <div className="noteBtn text-center">
            <p className="">
              By clicking "Pay", You agree to Ryets'
              <Link to=""> Terms and conditions</Link>
            </p>
            <button disabled={!this.props.stripe}>Confirm order</button>
          </div>
        </form>
      </div>
    );
  }
}

function InjectedCheckoutForm() {
  return (
    <ElementsConsumer>
      {({ stripe, elements }) => (
        <CheckoutForm stripe={stripe} elements={elements} />
      )}
    </ElementsConsumer>
  );
}
export default withRouter(InjectedCheckoutForm);
