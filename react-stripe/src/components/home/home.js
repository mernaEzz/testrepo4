import React from "react";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import Payment from "../payment/payment";
const stripePromise = loadStripe(
  "pk_test_51HyyMWGfs8Uw7aLgNl6CO44NKe4R9o4ogiwoTTOdQ4l4NU0RPkoaKqekrco3JsdITWKVrvVf7mt12pOZPp3Ln4ji00bk0mvwB1"
);

const Home = () => {
  return (
    <div>
      <Elements stripe={stripePromise}>
        <Payment />
      </Elements>
    </div>
  );
};
export default Home;
