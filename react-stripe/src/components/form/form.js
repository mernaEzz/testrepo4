import { extend } from "jquery";
import React from "react";
import "./form.css";

class Form extends React.Component {
  render() {
    return (
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" class="form-control" id="usr" required />
              <label for="usr">Name</label>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="password" required />
              <label for="usr">Password</label>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Form
