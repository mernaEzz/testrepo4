import React from "react";
import { loadStripe } from "@stripe/stripe-js";
import {
  CardCvcElement,
  CardElement,
  CardExpiryElement,
  CardNumberElement,
  Elements,
  ElementsConsumer,
} from "@stripe/react-stripe-js";
import "./stripePayment.css";
import { Link } from "react-router-dom";

import { prefixes } from "../../../src/prefixes.js";
import Visa from "../../images/Group 1905@2x.png";
import Mastercard from "../../images/Group 1906.png";
import Amex from "../../images/Mask Group 7.png";
import Discover from "../../images/Group 1908.png";
const Logo = ({ type, alt, active }) => {
  let imgClass = "ximg";

  if (active) {
    imgClass = "ximg  active";
  }

  return (
    <>
      <img src={type} alt={`${alt} credit card logo`} className={imgClass} />
    </>
  );
};

const CARD_OPTIONS = {
  iconStyle: "solid",
  style: {
    base: {
      iconColor: "#c4f0ff",
      color: "black",
      fontWeight: 500,
      fontFamily: "Roboto, Open Sans, Segoe UI, sans-serif",
      fontSize: "16px",
      fontSmoothing: "antialiased",
      ":-webkit-autofill": {
        color: "black",
      },
      ".StripeElement--focus:valid~label": {
        bottom: "55px",
      },
      "::placeholder": {
        color: "transparent",
      },
    },

    invalid: {
      iconColor: "red",
      color: "red",
    },
  },
};

const CardField = ({ onChange }) => (
  <div className="form-group">
    <CardNumberElement
      options={CARD_OPTIONS}
      onChange={onChange}
      className="form-control"
    />
    <label>Card Number</label>
  </div>
);

const CardField2 = ({ onChange }) => (
  <div className="form-group">
    <CardExpiryElement
      options={CARD_OPTIONS}
      onChange={onChange}
      className="form-control"
    />
    <label>Expiry Date</label>
  </div>
);

const CardField3 = ({ onChange }) => (
  <div className="form-group">
    <CardCvcElement
      options={CARD_OPTIONS}
      onChange={onChange}
      className="form-control"
    />
    <label>CVV</label>
  </div>
);
const Field = ({
  label,
  id,
  type,
  placeholder,
  required,
  autoComplete,
  value,
  onChange,
  className,
}) => (
  <div className="form-group">
    <input
      id={id}
      type={type}
      required={required}
      autoComplete={autoComplete}
      value={value}
      onChange={onChange}
      className={className}
    />
    <label htmlFor={id}>{label}</label>
  </div>
);

const SubmitButton = ({ processing, error, children, disabled }) => (
  <button
    className={`SubmitButton ${error ? "SubmitButton--error" : ""}`}
    type="submit"
    disabled={processing || disabled}
  >
    {processing ? "Processing..." : children}
  </button>
);

const ErrorMessage = ({ children }) => (
  <div className="ErrorMessage" role="alert">
    {children}
  </div>
);

const ResetButton = ({ onClick }) => (
  <button type="button" className="ResetButton" onClick={onClick}>
    <svg width="32px" height="32px" viewBox="0 0 32 32">
      <path
        fill="#FFF"
        d="M15,7.05492878 C10.5000495,7.55237307 7,11.3674463 7,16 C7,20.9705627 11.0294373,25 16,25 C20.9705627,25 25,20.9705627 25,16 C25,15.3627484 24.4834055,14.8461538 23.8461538,14.8461538 C23.2089022,14.8461538 22.6923077,15.3627484 22.6923077,16 C22.6923077,19.6960595 19.6960595,22.6923077 16,22.6923077 C12.3039405,22.6923077 9.30769231,19.6960595 9.30769231,16 C9.30769231,12.3039405 12.3039405,9.30769231 16,9.30769231 L16,12.0841673 C16,12.1800431 16.0275652,12.2738974 16.0794108,12.354546 C16.2287368,12.5868311 16.5380938,12.6540826 16.7703788,12.5047565 L22.3457501,8.92058924 L22.3457501,8.92058924 C22.4060014,8.88185624 22.4572275,8.83063012 22.4959605,8.7703788 C22.6452866,8.53809377 22.5780351,8.22873685 22.3457501,8.07941076 L22.3457501,8.07941076 L16.7703788,4.49524351 C16.6897301,4.44339794 16.5958758,4.41583275 16.5,4.41583275 C16.2238576,4.41583275 16,4.63969037 16,4.91583275 L16,7 L15,7 L15,7.05492878 Z M16,32 C7.163444,32 0,24.836556 0,16 C0,7.163444 7.163444,0 16,0 C24.836556,0 32,7.163444 32,16 C32,24.836556 24.836556,32 16,32 Z"
      />               
    </svg>
  </button>
);

const DEFAULT_STATE = {
  error: null,
  cardComplete: false,
  processing: false,
  paymentMethod: null,
  email: "",
  name: "",
  errors: [],
  CardNumber: "",
};
class CheckoutForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = DEFAULT_STATE;
  }

  hasError(key) {
    return this.state.errors.indexOf(key) !== -1;
  }

  handleSubmit = async (event) => {
    event.preventDefault();

    const { stripe, elements } = this.props;
    const { email, name, error, cardComplete } = this.state;
    var errors = [];
    if (this.state.name === "") {
      errors.push("name");
    }

    const expression = /\S+@\S+/;
    var validEmail = expression.test(String(this.state.email).toLowerCase());

    if (!validEmail) {
      errors.push("email");
    }
    if (!stripe || !elements) {
      // Stripe.js has not loaded yet. Make sure to disable
      // form submission until Stripe.js has loaded.
      return;
    }

    if (error) {
      //   elements.getElement('card').focus();
      return;
    }

    if (cardComplete) {
      this.setState({ processing: true });
    }

    const cardElement = elements.getElement(CardNumberElement);

    const payload = await stripe.createPaymentMethod({
      type: "card",
      card: cardElement,
      billing_details: {
        email,
        name,
      },
    });

    this.setState({ processing: false, errors: errors });

    if (payload.error) {
      this.setState({ error: payload.error });
      console.log(payload.error);
    } else {
      this.setState({ paymentMethod: payload.paymentMethod });
      console.log(this.state.paymentMethod);
    }
  };

  reset = () => {
    this.setState(DEFAULT_STATE);
  };
  render() {
    const { error, processing, paymentMethod, name, email } = this.state;
    const { stripe } = this.props;
    const { errors } = this.state;

    return paymentMethod ? (
      <div className="Result">
        <div className="ResultTitle" role="alert">
          Payment successful
        </div>
        <div className="ResultMessage">
          Thanks for trying Stripe Elements. No money was charged, but we
          generated a PaymentMethod: {paymentMethod.id}
        </div>
        <ResetButton onClick={this.reset} />
      </div>
    ) : (
      <div className="col-lg-6 col-md-6">
        <div className="stripePayment2">
          <div className="payHeader ">
            <button className="container">
              <i className="fa fa-apple"></i> Pay
            </button>
          </div>
          <div className="or">
            <p>Or pay with card</p>
          </div>
          <form className="Form" onSubmit={this.handleSubmit} noValidate>
            <fieldset className="FormGroup">
              <div className="input-group">
                <Field
                  label="Email"
                  id="email"
                  type="text"
                  required
                  className={
                    this.hasError("email")
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  name="email"
                  autoComplete="email"
                  value={paymentMethod}
                  onChange={(event) => {
                    this.setState({
                      error: event.error,
                      cardComplete: event.complete,
                      email: event.target.value,
                    });
                  }}
                />
                <div
                  className={
                    this.hasError("email") ? "inline-errormsg" : "hidden"
                  }
                >
                  Email is invalid or missing
                </div>
              </div>
              <div className="input-group">
                <div className="input-group-append">
                  <CardField
                    type="text"
                    value={paymentMethod}
                    onChange={(event) => {
                      this.setState({
                        error: event.error,
                        cardComplete: event.complete,
                      });
                    }}
                  />
                  <Logo type={Visa} alt="Visa" active={this.state.activeVisa} />
                  <Logo
                    type={Mastercard}
                    alt="Mastercard"
                    active={this.state.activeMastercard}
                  />
                  <Logo
                    type={Amex}
                    alt="American Express"
                    active={this.state.activeAmex}
                  />
                  <Logo
                    type={Discover}
                    alt="Discover"
                    active={this.state.activeDiscover}
                  />
                </div>
              </div>
              <div className="input-group">
                <Field
                  label="Name on Card"
                  id="name"
                  type="text"
                  name="name"
                  required
                  className={
                    this.hasError("name")
                      ? "form-control is-invalid"
                      : "form-control"
                  }
                  placeholder="Jane Doe"
                  autoComplete="name"
                  value={paymentMethod}
                  onChange={(event) => {
                    this.setState({
                      error: event.error,
                      cardComplete: event.complete,
                      name: event.target.value,
                    });
                  }}
                />
                <div
                  className={
                    this.hasError("name") ? "inline-errormsg" : "hidden"
                  }
                >
                  Please enter a value
                </div>
              </div>
            </fieldset>
            <fieldset className="FormGroup row">
              <div className="col-md-6">
                <div className="input-group">
                  <CardField2
                    onChange={(event) => {
                      this.setState({
                        error: event.error,
                        cardComplete: event.complete,
                      });
                    }}
                  />
                </div>
              </div>
              <div className="col-md-6">
                <div className="input-group">
                  <div className="input-group-append">
                    <CardField3
                      onChange={(event) => {
                        this.setState({
                          error: event.error,
                          cardComplete: event.complete,
                        });
                      }}
                    />
                    <div className="items">
                      <a href="#" class="has-tooltip">
                        <img src="Mask Group 8.svg" id="example" />
                        <div className="container">
                          <span class="tooltip tooltip-top ">
                            is the three-digit number at the back of your debit
                            card. For certain types of debit cards, it could be
                            a four-digit number printed on the front
                          </span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </fieldset>
            <div className="noteBtn text-center">
              <p className="">
                By clicking "Pay", You agree to Ryets' 
                <Link to=""> Terms and conditions</Link>
              </p>
              {error && <ErrorMessage>{error.message}</ErrorMessage>}

              <SubmitButton
                processing={processing}
                error={error}
                disabled={!stripe}
              >
                Pay $25
              </SubmitButton>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const InjectedCheckoutForm = () => (
  <ElementsConsumer>
    {({ stripe, elements }) => (
      <CheckoutForm stripe={stripe} elements={elements} />
    )}
  </ElementsConsumer>
);

const ELEMENTS_OPTIONS = {
  fonts: [
    {
      cssSrc: "https://fonts.googleapis.com/css?family=Roboto",
    },
  ],
};

// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe("pk_test_6pRNASCoBOKtIshFeQd4XMUh");

const StripePayment = () => {
  return (
    <div className="AppWrapper">
      <Elements stripe={stripePromise} options={ELEMENTS_OPTIONS}>
        <InjectedCheckoutForm />
      </Elements>
    </div>
  );
};

export default StripePayment;
