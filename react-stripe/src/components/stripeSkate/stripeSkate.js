import React, { useEffect } from "react";
import { Link, history } from "react-router-dom";
import "./stripeSkate.css";
import $ from "jquery";
import tooltip from "tooltip";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import payform from 'payform/dist/payform.min.js';
 

import { withRouter } from "react-router-dom";

import { prefixes } from "../../../src/prefixes.js";
import Visa from "../../images/Group 1905@2x.png";
import Mastercard from "../../images/Group 1906.png";
import Amex from "../../images/Mask Group 7.png";
import Discover from "../../images/Group 1908.png";
const Logo = ({ type, alt, active }) => {
  let imgClass = 'x';

  if (active) {
    imgClass = 'x  active';
  }

  return (
    <>
      <img src={type} alt={`${alt} credit card logo`} className={imgClass} />
    </>
  );
}

class StripeSkate extends React.Component {
  componentDidMount(){
    window.scrollTo(0,0)
  }
  state = {
    email: "",
    name: "",
    errors: "",
    expiry: "",
    cvv:"",
    maxLength: 16,
    cardNumber: '',
    activeVisa: false, // TODO: Clean this up
    activeMastercard: false,
    activeDiscover: false,
    activeAmex: false,
    type: '',
    valid: '',
    error: {},
   };
  getValidMessage = () => {
    if (this.state.valid !== '') {
      return this.state.valid
        ? 'Valid ✓'
        : 'Credit card number is invalid';
    }

    return '';
  }
  verifyNumber = () => {
    let sum = 0;
    let temp = 0;
    let cardNumberCopy = this.state.cardNumber;
    let checkDigit = parseInt(this.state.cardNumber.slice(-1));
    let parity = cardNumberCopy.length % 2;

    for (let i = 0; i <= cardNumberCopy.length - 2; i++) {
      if (i % 2 === parity) {
        temp = (+cardNumberCopy[i]) * 2;
      }
      else {
        temp = (+cardNumberCopy[i]);
      }

      if (temp > 9) {
        temp -= 9;
      }

      sum += temp;
    }

    return (sum + checkDigit) % 10 === 0;
  }
  purgeInactive = (firstCard, secondCard, thirdCard, fourthCard) => {
    this.setState({
      ['active' + firstCard]: false,
      ['active' + secondCard]: false,
      ['active' + thirdCard]: false,
      ['active' + fourthCard]: true,
      valid: '',
    });
  }
  determineType = (cardNumber) => {

    for (let key of prefixes) {
      for (let value of key[1]) {
        if (cardNumber.startsWith(value)) {
          this.setState({
            type: key[0],
          });

          /* TODO: Find a better way to manage this. */
          switch (key[0]) {
            case 'Visa':
              this.purgeInactive('Mastercard', 'Discover', 'Amex', 'Visa');
              break;
            case 'Mastercard':
              this.purgeInactive('Visa', 'Discover', 'Amex', 'Mastercard');
              break;
            case 'Discover':
              this.purgeInactive('Visa', 'Mastercard', 'Amex', 'Discover');
              break;
            case 'Amex':
              this.purgeInactive('Visa', 'Mastercard', 'Discover', 'Amex');
              break;
            default:
              break;
          }

          return;
        }
        else {
          this.setState({
            ['active' + key[0]]: false,
            type: '',
            valid: '',
          });
        }
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.cardNumber !== this.state.cardNumber) {
      this.determineType(this.state.cardNumber);
    }

    if (prevState.activeAmex !== this.state.activeAmex) {
      this.state.activeAmex
        ? this.setState({ maxLength: 15 })
        : this.setState({ maxLength: 16 });
    }

    if (prevState.type !== this.state.type) {
      if (this.state.type !== '') {
        this.setState({
          ['active' + this.state.type]: true,
        });
      }
    }

    /* A chain like this just seems wrong. */
    if (prevState.cardNumber.length !== this.state.cardNumber.length
        && this.state.cardNumber.length === this.state.maxLength) {
          this.setState({
            valid: this.verifyNumber(),
          });
    }
  }

  handleChangess = (e) => {
    this.setState({
      cardNumber: e.target.value
    });
  }

  handleClick = (e) => {
    this.setState({
      cardNumber: '',
      valid: '',
    });
  }

  formatString = (event) => {
    var inputChar = String.fromCharCode(event.keyCode);
    var code = event.keyCode;
    var allowedKeys = [8];
    if (allowedKeys.indexOf(code) !== -1) {
      return;
    }

    event.target.value = event.target.value
      .replace(
        /^([1-9]\/|[2-9])$/g,
        "0$1/" // 3 > 03/
      )
      .replace(
        /^(0[1-9]|1[0-2])$/g,
        "$1/" // 11 > 11/
      )
      .replace(
        /^([0-1])([3-9])$/g,
        "0$1/$2" // 13 > 01/3
      )
      .replace(
        /^(0?[1-9]|1[0-2])([0-9]{2})$/g,
        "$1/$2" // 141 > 01/41
      )
      .replace(
        /^([0]+)\/|[0]+$/g,
        "0" // 0/ > 0 and 00 > 0
      )
      .replace(
        /[^\d\/]|^[\/]*$/g,
        "" // To allow only digits and `/`
      )
      .replace(
        /\/\//g,
        "/" // Prevent entering more than 1 `/`
      );
  };

  handleChange = (event) => {
    const email = event.target.value;
    this.setState({ email });
  };
  handleChange2 = (event) => {
    const name = event.target.value;
    this.setState({ name });

    
  };
  handleChange3 = (event) => {
    const expiry = event.target.value;
    this.setState({ expiry });
    
  };

  handleChange4 = (event) => {
    const cvv = event.target.value;
    this.setState({ cvv });
    
  };
  handleSubmit = () => {
    // your submit logic
    console.log(this.state.email);
  };
  sub=()=>{
    this.props.history.push('/paymentForInvoice')
  }
  render() {
    const { email, name, expiry,cvv,type,maxLength } = this.state;

    return (
      <div className="payment col-md-6">
        <div className="payHeader ">
          <button className="container" onClick={()=>this.sub()}>
            <i className="fa fa-apple"></i> Pay
          </button>
        </div>
        <div className="or">
          <p>Or pay with card</p>
        </div>
        <div className="form">
          <ValidatorForm ref="form" onSubmit={this.handleSubmit}>
            <div class="form-group">
              <TextValidator
                id="standard-basic"
                label="Email"
                type="email"
                fullWidth="true"
                onChange={this.handleChange}
                value={email}
                validators={["required", "isEmail"]}
                errorMessages={["this field is required", "email is not valid"]}
              />
            </div>

            <div class="form-group">
              <div className="input-group">
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item fullWidth="true">
                    <TextValidator
                      id="standard-basic2"
                      className="txt"
                      label="Card Number"
                      type="text"
                      fullWidth="true"
                      
                      validators={["required"]}
                      onChange={this.handleChangess}
                      value={this.state.cardNumber}
                      errorMessages={["this field is required"]}
                      InputProps={{
                        inputProps: {maxLength} ,
                        endAdornment: (
                          <div className="item">
                        


            <Logo type={Visa} 
            alt="Visa"
            active={this.state.activeVisa}
          />
          <Logo type={Mastercard} 
            alt= "Mastercard"
            active={this.state.activeMastercard}
          />
           <Logo type={Amex}
            alt="American Express"
            active={this.state.activeAmex}
          />
          <Logo type={Discover}
            alt="Discover"
            active={this.state.activeDiscover}
          />
         
                          </div>
                        ),
                      }}
                    />
                     <span className=
            { this.state.valid? 'error valid' : 'error invalid' }>
              { this.getValidMessage() }
          </span>
                  </Grid>
                </Grid>
              </div>
            </div>
            <div class="form-group md-form">
              <TextValidator
                label="Name On Card"
                type="text"
                fullWidth="true"
                onChange={this.handleChange2}
                value={name}
                validators={["required","matchRegexp:^[a-zA-Z][a-zA-Z0-9-_\.]{0,50}$"]}
                errorMessages={["this field is required","invalid name(can't start with number,use - or _ instead,must be less than 50)"]}
              />
              <p style={{ color: "red" }}>{this.state.errors}</p>
            </div>
            <div className="row">
              <div class="form-group col-md-6">
                <TextValidator
                  label="Exp.Date"
                  type="text"
                  fullWidth="true"
                  onChange={this.handleChange3}
                  value={expiry}
                  onKeyUp={(event) => this.formatString(event)}
                  InputProps={{ 
                    inputProps: { maxLength: 5} 
                 }}
                  validators={["required",'matchRegexp:([0-9]{2}[/]?){2}']}
                  errorMessages={["this field is required","expiry date incomplete"]}
                />
                <p style={{ color: "red" }}>{this.state.expiryError}</p>
              </div>
              <div class="form-group col-md-6 ">
                <div className="input-group">
                  <Grid container spacing={1} alignItems="flex-end">
                    <Grid item fullWidth="true">
                      <TextValidator
                        id="standard-basic"
                        className="txt"
                        label="CVV"
                        type="text"
                        fullWidth="true"
                         
                        onChange={this.handleChange4}
                        value={cvv}
                        validators={["required", "matchRegexp:([0-9]{2}[/]?){2}"]}
                        errorMessages={["this field is required",
                        "Your card's security code is incomplete.(invalid number)"]}
                        InputProps={{
                          inputProps: { maxLength: 4} ,
                          endAdornment: (
                            <div className="items">
                              <a href="#" class="has-tooltip">
                                <img src="Mask Group 8.svg" id="example" />
                                <div className="container">
                                  <span class="tooltip tooltip-top ">
                                    t is the three-digit number at the back of
                                    your debit card. For certain types of debit
                                    cards, it could be a four-digit number
                                    printed on the front
                                  </span>
                                </div>
                              </a>
                            </div>
                          ),
                        }}
                      />
                    </Grid>
                  </Grid>
                </div>
              </div>
            </div>
            <div className="noteBtn text-center">
              <p className="">
                By clicking "Pay", You agree to Ryets'
                <Link to=""> Terms and conditions</Link>
              </p>
              <button className="container"   onClick={()=>this.props.history.push('/successPayment')}>Pay $ 2,520.00</button>
            </div>
          </ValidatorForm>
        </div>
      </div>
    );
  }
}
export default withRouter(StripeSkate)
