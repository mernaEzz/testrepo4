import React from "react";
import "./carInfo.css";
import copy from "copy-to-clipboard";
import $ from "jquery";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class CarInfo extends React.Component {
  
  constructor() {
    super();
    this.state = {
      textToCopy: "2019 MercedesBenz C180",
      text2:'2G1WT57N591216081',
      customId : "custom-id-yes",
      customId2 : "custom-id-yes2",
    };
    this.Copytext = this.Copytext.bind(this);
  }
  Copytext() {
   
    copy(this.state.textToCopy);
    toast("copied",{
      toastId: this.state.customId
     });
  }
  Copytext2() {
    copy(this.state.text2);
    toast("copied",{
      toastId: this.state.customId2
    });
  }
  render() {
    return (
      <div className="carInfo">
        <div className="carName row">
          <p>{this.state.textToCopy}</p>
          <img src="Mask Group 120.svg" className="m" onClick={this.Copytext} />
          
        </div>
        <div className="carData row">
          <p>{this.state.text2}</p>
          <img src="Mask Group 120.svg" onClick={()=>this.Copytext2()}/>
        </div>
        <div className="info row">
          <div className="item">
            <p>6527213</p>
            <span>LOT#</span>
          </div>
          <div className="item">
            <div className="row">
              <p>Dark Grey</p>
              <div className="color"></div>
            </div>
            <span>Color</span>
          </div>
          <div className="item">
            <p>6527213</p>
            <span>LOT#</span>
          </div>
        </div>
      
          <div className="icons">
            <div className="">
              <ul>
              <li className=" ">2 <img src="Mask Group 1172.svg"/></li>
              <li className=" "><img src="Group 8.svg"/></li>
              <li className=" ">7 <img src="Mask Group 1147.svg"/></li>
              <li className=" ">3 <img src="Mask Group 122.svg"/></li>
              <li className=" "><img src="Group 195.svg"/></li>
              <li className=" "><img src="Group 7.svg"/></li>
              </ul>
            </div>
          </div>
      </div>
    );
  }
}
export default CarInfo;
