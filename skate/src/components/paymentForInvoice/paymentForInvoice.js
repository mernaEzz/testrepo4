import React, { useEffect } from 'react';
import Invoice from '../invPayment/invoice/invoice';
import Payment from '../invPayment/payment/payment';
import './paymentForInvoice.css'
const PaymentForInvoice=()=>{
    useEffect(()=>{
        window.scrollTo(0,0)
    })
    return(
        <div className="paymentForInvoice">
            <div className="container" id="container" >
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                        <Invoice/>
                        <div className="border-dashed container"></div>
                        <Payment/>
                    </div>
                   
                </div>
            </div>
        </div>
    )
}
export default PaymentForInvoice